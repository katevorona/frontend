import React, { Component } from 'react';
import axios from 'axios';
import './FileZone.css';
import mockText from '../mockText'

const API_URL = 'https://api.datamuse.com/words?rel_syn='

const COUNT_SYMBOL_IN_OPEN_TAG = 3
const COUNT_SYMBOL_IN_CLOSE_TAG = 4

const BOLD_TAG = 'bold'
const ITALIC_TAG = 'italic'
const UNDERLINE_TAG = 'underline'

const TAGS = {
  [BOLD_TAG]: {
    'open': '<b>',
    'close': '</b>'
  },
  [ITALIC_TAG]: {
    'open': '<i>',
    'close': '</i>'
  },
  [UNDERLINE_TAG]: {
    'open': '<u>',
    'close': '</u>'
  }
}

class FileZone extends Component {

  constructor(props) {
    super(props)
    this.state = {
      textZone: mockText,
      synonyms: [],
      synonymShow: false,
      startPosition: 0,
      endPosition: 0
    }
  }

  countOpenTagsSymbols(text) {
    return ((text.split("<b>").length-1) + (text.split("<u>").length-1) + (text.split("<i>").length-1)) * COUNT_SYMBOL_IN_OPEN_TAG
  }

  countCloseTagsSymbols(text) {
    return ((text.split("</b>").length-1) + (text.split("</u>").length-1) + (text.split("</i>").length-1)) * COUNT_SYMBOL_IN_CLOSE_TAG
  }

  calculateStartEndPosition() {
    const win = this.refs.textZone.ownerDocument.defaultView;
    const range = win.getSelection().getRangeAt(0);
    const preCaretRange = range.cloneRange();
    preCaretRange.selectNodeContents(this.refs.textZone);
    preCaretRange.setEnd(range.endContainer, range.endOffset);

    const endPosit = preCaretRange.toString().length;

    let textBeforeEndPosition = this.state.textZone.toString().substring(0, endPosit);
    let endPosition = endPosit + this.countOpenTagsSymbols(textBeforeEndPosition) + this.countCloseTagsSymbols(textBeforeEndPosition);

    const selectedPart = window.getSelection();
    let startPosition = endPosition-selectedPart.toString().length;

    const selectedText = this.state.textZone.toString().substring(startPosition, endPosition)

    if(this.countOpenTagsSymbols(selectedText) > 0) {
      startPosition = startPosition + this.countOpenTagsSymbols(selectedText)
      endPosition = endPosition + this.countOpenTagsSymbols(selectedText)
    }

    return {
      startPosition,
      endPosition
    }
  }

  findSynonyms() {
    const word = window.getSelection().toString()
    const {startPosition, endPosition} = this.calculateStartEndPosition();

    axios.get(`${API_URL}${word}`)
      .then((response) => (
        this.setState({startPosition, endPosition, synonymShow: true, synonyms: response.data.map(synonym => synonym.word)})
      ))
  }

  closeSynonyms() {
    this.setState({synonymShow: false})
  }


  changeSynonyms(synonym) {
    const allText = this.state.textZone.toString();
    const {startPosition, endPosition} = this.state;

    let textBefore = allText.substring(0, startPosition);
    let textAfter = allText.substring(endPosition, allText.length);

    this.setState({synonymShow: false, textZone: `${textBefore}${synonym}${textAfter}`})
  }

  transformText(tag) {
    const allText = this.state.textZone.toString();
    const {startPosition, endPosition} = this.calculateStartEndPosition();

    let textBefore = allText.substring(0, startPosition);
    let selectedText = allText.substring(startPosition, endPosition);
    let textAfter = allText.substring(endPosition, allText.length);

    const newText = this.getResultText(textBefore, selectedText, textAfter, tag);

    this.setState({textZone: newText})
  }

  //<u><i><b></b></i></u>
  // sorry for code bellow, it's awful i know
  getResultText(textBefore, selectedText, textAfter, tag) {
    const endWithIAndB = `${TAGS[ITALIC_TAG].open}${TAGS[BOLD_TAG].open}`;
    const startsWithIAndB = `${TAGS[BOLD_TAG].close}${TAGS[ITALIC_TAG].close}`;
    const endWithUAndB = `${TAGS[UNDERLINE_TAG].open}${TAGS[BOLD_TAG].open}`;
    const startsWithUAndB = `${TAGS[BOLD_TAG].close}${TAGS[UNDERLINE_TAG].close}`;
    const endWithUAndI = `${TAGS[UNDERLINE_TAG].open}${TAGS[ITALIC_TAG].open}`;
    const startsWithUAndI = `${TAGS[ITALIC_TAG].close}${TAGS[UNDERLINE_TAG].close}`;
    const endWithAll = `${TAGS[UNDERLINE_TAG].open}${TAGS[ITALIC_TAG].open}${TAGS[BOLD_TAG].open}`;
    const startsWithAll = `${TAGS[BOLD_TAG].close}${TAGS[ITALIC_TAG].open}${TAGS[UNDERLINE_TAG].close}`;

    const tagsOpen = textBefore.substring(textBefore.length-COUNT_SYMBOL_IN_OPEN_TAG, textBefore.length)
    const tagsClose = textAfter.substring(0, COUNT_SYMBOL_IN_CLOSE_TAG)

    const twoOpenTags = textBefore.substring(textBefore.length-COUNT_SYMBOL_IN_OPEN_TAG*2, textBefore.length)
    const twoCloseTags = textAfter.substring(0, COUNT_SYMBOL_IN_CLOSE_TAG*2)

    const threeOpenTags = textBefore.substring(textBefore.length-COUNT_SYMBOL_IN_OPEN_TAG*3, textBefore.length)
    const threeCloseTags = textAfter.substring(0, COUNT_SYMBOL_IN_CLOSE_TAG*3)

    if(textBefore.endsWith(TAGS[tag].open) && textAfter.startsWith(TAGS[tag].close)) {
      textBefore = this.getTextBefore(textBefore, tagsOpen, [])
      textAfter = this.getTextAfter(textAfter, tagsClose, [])
    }
    else if(textBefore.endsWith(endWithAll) && textAfter.startsWith(startsWithAll)) {
      const subsOpenTags = threeOpenTags.split(TAGS[tag].open).join("")
      const subsCloseTags = threeCloseTags.split(TAGS[tag].close).join("")

      textBefore = `${textBefore.substring(0, textBefore.length-threeOpenTags.length)}${subsOpenTags}`;
      textAfter = `${subsCloseTags}${textAfter.substring(threeCloseTags.length, textAfter.length)}`;
    }
    else if(textBefore.endsWith(endWithIAndB) && textAfter.startsWith(startsWithIAndB)) {
      switch (tag) {
        case ITALIC_TAG:
          textBefore = this.getTextBefore(textBefore, twoOpenTags, [BOLD_TAG])
          textAfter = this.getTextAfter(textAfter, twoCloseTags, [BOLD_TAG])
          break;
        case UNDERLINE_TAG:
          textBefore = this.getTextBefore(textBefore, twoOpenTags, [UNDERLINE_TAG, ITALIC_TAG, BOLD_TAG])
          textAfter = this.getTextAfter(textAfter, twoCloseTags, [BOLD_TAG, ITALIC_TAG, UNDERLINE_TAG])
          break;
      }
    }
    else if(textBefore.endsWith(endWithUAndB) && textAfter.startsWith(startsWithUAndB)) {
      switch (tag) {
        case ITALIC_TAG:
          textBefore = this.getTextBefore(textBefore, twoOpenTags, [UNDERLINE_TAG, ITALIC_TAG, BOLD_TAG])
          textAfter = this.getTextAfter(textAfter, twoCloseTags, [BOLD_TAG, ITALIC_TAG, UNDERLINE_TAG])
          break;
        case UNDERLINE_TAG:
          textBefore = this.getTextBefore(textBefore, twoOpenTags, [BOLD_TAG])
          textAfter = this.getTextAfter(textAfter, twoCloseTags, [BOLD_TAG])
          break;
      }
    }
    else if(textBefore.endsWith(endWithUAndI) && textAfter.startsWith(startsWithUAndI)) {
      switch (tag) {
        case BOLD_TAG:
          textBefore = this.getTextBefore(textBefore, twoOpenTags, [UNDERLINE_TAG, ITALIC_TAG, BOLD_TAG])
          textAfter = this.getTextAfter(textAfter, twoCloseTags, [BOLD_TAG, ITALIC_TAG, UNDERLINE_TAG])
          break;
        case ITALIC_TAG:
          textBefore = this.getTextBefore(textBefore, twoOpenTags, [UNDERLINE_TAG])
          textAfter = this.getTextAfter(textAfter, twoCloseTags, [UNDERLINE_TAG])
          break;
        case UNDERLINE_TAG:
          textBefore = this.getTextBefore(textBefore, twoOpenTags, [ITALIC_TAG])
          textAfter = this.getTextAfter(textAfter, twoCloseTags, [ITALIC_TAG])
          break;
      }
    }
    else if(textBefore.endsWith(TAGS[BOLD_TAG].open) && textAfter.startsWith(TAGS[BOLD_TAG].close) && tag !== BOLD_TAG) {
      textBefore = this.getTextBefore(textBefore, tagsOpen, [tag, BOLD_TAG])
      textAfter = this.getTextAfter(textAfter, tagsClose, [BOLD_TAG, tag])
    }
    else if(textBefore.endsWith(TAGS[ITALIC_TAG].open) && textAfter.startsWith(TAGS[ITALIC_TAG].close)) {
      switch (tag) {
        case BOLD_TAG:
          textBefore = this.getTextBefore(textBefore, tagsOpen, [ITALIC_TAG, BOLD_TAG])
          textAfter = this.getTextAfter(textAfter, tagsClose, [BOLD_TAG, ITALIC_TAG])
          break;
        case UNDERLINE_TAG:
          textBefore = this.getTextBefore(textBefore, tagsOpen, [UNDERLINE_TAG, ITALIC_TAG])
          textAfter = this.getTextAfter(textAfter, tagsClose, [ITALIC_TAG, UNDERLINE_TAG])
          break;
      }
    }
    else if(textBefore.endsWith(TAGS[UNDERLINE_TAG].open) && textAfter.startsWith(TAGS[UNDERLINE_TAG].close)) {
      switch (tag) {
        case BOLD_TAG:
          textBefore = this.getTextBefore(textBefore, tagsOpen, [UNDERLINE_TAG, BOLD_TAG])
          textAfter = this.getTextAfter(textAfter, tagsClose, [BOLD_TAG, UNDERLINE_TAG])
          break;
        case ITALIC_TAG:
          textBefore = this.getTextBefore(textBefore, tagsOpen, [UNDERLINE_TAG, ITALIC_TAG])
          textAfter = this.getTextAfter(textAfter, tagsClose, [ITALIC_TAG, UNDERLINE_TAG])
          break;
      }
    }
    else {
      selectedText = `${TAGS[tag].open}${selectedText}${TAGS[tag].close}`
    }

    return `${textBefore}${selectedText}${textAfter}`;
  }

  getTextBefore(text, openTags, tags) {
    const openTagsArray = tags.map(tag => TAGS[tag].open)
    return `${text.substring(0, text.length-openTags.length)}${openTagsArray.join('')}`
  }

  getTextAfter(text, closeTags, tags) {
    const closeTagsArray = tags.map(tag => TAGS[tag].close)
    return `${closeTagsArray.join('')}${text.substring(closeTags.length, text.length)}`
  }

  renderControlPanel() {
    return (
      <div id="control-panel">
        <div id="format-actions">
          <button
            className="format-action"
            type="button"
            onClick={() => { this.transformText(BOLD_TAG) }}
          >
            <b>B</b>
          </button>
          <button
            className="format-action"
            type="button"
            onClick={() => { this.transformText(ITALIC_TAG) }}
          >
            <i>I</i>
          </button>
          <button
            className="format-action"
            type="button"
            onClick={() => { this.transformText(UNDERLINE_TAG) }}
          >
            <u>U</u>
          </button>
          <button
            className="format-action"
            type="button"
            onClick={() => { this.findSynonyms() }}
          >
            Synonyms
          </button>
          {this.renderModalWindowWithSynonyms()}
        </div>
      </div>
    )
  }

  renderModalWindowWithSynonyms() {
    return (
      <div className={`Synonyms ${this.state.synonymShow ? 'active' : ''}`}>

        <div className="title">Synonyms for word '{window.getSelection().toString()}'</div>

        {this.state.synonyms.length > 0 ? this.state.synonyms.map(synonym => (
          <span key={synonym} onClick={() => { this.changeSynonyms(synonym) }}> {synonym} </span>
        )) :
        <div> Synonyms is not found</div>
        }
        <div className="closeButton" onClick={() => { this.closeSynonyms() }}> x </div>
      </div>
    )
  }

  render() {
    return (
      <div id="file-zone">
        {this.renderControlPanel()}
        <div
          id="file"
          ref='textZone'
          dangerouslySetInnerHTML={{__html: this.state.textZone}}
        >
        </div>
      </div>
    );
  }
}

export default FileZone;
